function is_jailed() {
	local res
	local data
	local verbose

	verbose=0
	while getopts v o; do
		case "${o}" in
			v)
				verbose=$((${verbose} + 1))
		esac
	done

	data=$(sysctl -n security.jail.jailed 2>/dev/null)
	res=${?}
	if [ ${verbose} -gt 0 ]; then
		echo ${data}
	fi
	if [ ${res} -eq 0 ]; then
		if [ ${data} -eq 1 ]; then
			return 0
		fi
	fi
	return 1
}

